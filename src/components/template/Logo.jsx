import './Logo.css';
import Logo from '../../assets/imgs/logo.png'
import React from 'react'
import { Link } from 'react-router-dom'

 const PaginaLogo = props =>

    <aside className="logo">
        <Link to="/" clLinkssName="logo">
            <img src={Logo} alt={Logo}/>
        </Link>
    </aside>


export default PaginaLogo