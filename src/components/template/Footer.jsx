import './Footer.css'
import React from 'react'

const Footer = props =>

    <footer className="footer">
        <span>
            Desenvolvido com <i className="fa fa-heart text-danger"> </i> 
            por<strong> Christian <span className="text-danger">Silve</span>ira</strong>
</span>
    </footer>

export default Footer